﻿using Microsoft.EntityFrameworkCore;
using PortalAPI.Models;
using System;



namespace PortalAPI.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        //Values = table name
        public DbSet<Value> Values { get; set; }
        // table users
        public DbSet<User> Users { get; set; }
    }
}
